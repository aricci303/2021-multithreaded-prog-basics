package oop.concur;

import javax.swing.*;
import java.awt.event.*;

class MyFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public MyFrame() {
		super("Test Swing thread");
		setSize(200, 60);
		setVisible(true);
		JButton button = new JButton("test");
		button.addActionListener((ActionEvent ev) -> {
			/*
			while (true) {
			}*/
			try {
				Thread.sleep(500);
			} catch (Exception ex) {}
		});
		getContentPane().add(button);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}
		});
	}
}

public class MainSwingThread {
	static public void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new MyFrame();
			new MyFrame();
		});
	}
}
